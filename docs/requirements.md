# REQUIREMENTS

This document informs the list of requirements of the project.

* A system that captures purchase data from a caller.
* Store the data in a database.
* In case of a duplicate user, update their data instead of adding a new record.

Notice: There should not be duplicate records of a user in the database, meaning
that if a user data is different from his previous data, it should update their
record.

Notice: All purchase information should be stored in the database.
