# CALLER PURCHASES

An API that stores purchase data from a caller.

## Technology

* [ASP.NET CORE 6.0](https://dotnet.microsoft.com/en-us/apps/aspnet)

## License

[MIT](./LICENSE)

## Authors

See [AUTHORS](./AUTHORS) file.

## Contributors

See [CONTRIBUTORS](./CONTRIBUTORS) file.
