using Microsoft.EntityFrameworkCore;
using Models.Costumers;
using Models.Products;
using Models.Purchases;

namespace API.DAL;

public class ApiDbContext : DbContext
{
    public ApiDbContext(DbContextOptions<ApiDbContext> options) : base(options)
    {
        // Empty.
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<CostumerModel>()
            .HasMany(e => e.Products)
            .WithMany(e => e.Costumers)
            .UsingEntity<PurchaseModel>(
                r => r.HasOne<ProductModel>().WithMany().HasForeignKey(e => e.ProductId),
                l => l.HasOne<CostumerModel>().WithMany().HasForeignKey(e => e.CostumerId));
    }

    public DbSet<CostumerModel>? Costumers { get; set; }
    public DbSet<ProductModel>? Products { get; set; }
    public DbSet<PurchaseModel>? Purchases { get; set; }
}
