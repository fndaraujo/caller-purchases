namespace API.DAL.DTO.Costumers;

public class ReadCostumerDto
{
    public int Id { get; set; }

    public string Name { get; set; } = String.Empty;
    public string Surname { get; set; } = String.Empty;
    public DateTime Birth { get; set; }
    public string Email { get; set; } = String.Empty;
    public string Telephone { get; set; } = String.Empty;
    public string Mobile { get; set; } = String.Empty;
    public bool Active { get; set; } = true;
    public DateTime CreatedAt { get; set; } = DateTime.Now;
    public DateTime UpdatedAt { get; set; } = DateTime.Now;

    public DateTime ConsultedAt { get; set; } = DateTime.Now;
}
