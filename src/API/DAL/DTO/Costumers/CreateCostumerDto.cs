using System.ComponentModel.DataAnnotations;

namespace API.DAL.DTO.Costumers;

public class CreateCostumerDto
{
    [Required(ErrorMessage = "{0} is required.")]
    [StringLength(32, MinimumLength = 1, ErrorMessage = "{0} should be between {2} and {1} characters.")]
    public string Name { get; set; } = String.Empty;

    [Required(ErrorMessage = "{0} is required.")]
    [StringLength(256, MinimumLength = 1, ErrorMessage = "{0} should be between {2} and {1} characters.")]
    public string Surname { get; set; } = String.Empty;

    [Required(ErrorMessage = "{0} is required.")]
    [Range(typeof(DateTime), "1900-01-01", "2099-12-31", ErrorMessage = "{0} should be between {2} and {3} dates.")]
    public DateTime Birth { get; set; } = new();

    [Required(ErrorMessage = "{0} is required.")]
    [StringLength(254, ErrorMessage = "{0} should be {1} maximum characters.")]
    public string Email { get; set; } = String.Empty;

    [StringLength(10, ErrorMessage = "{0} should be {1} maximum characters.")]
    public string Telephone { get; set; } = String.Empty;

    [StringLength(11, ErrorMessage = "{0} should be {1} maximum characters.")]
    public string Mobile { get; set; } = String.Empty;

    [Required(ErrorMessage = "{0} is required.")]
    public bool Active { get; set; } = true;
}
