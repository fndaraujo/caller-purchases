namespace API.DAL.DTO.Purchases;

public class ReadPurchaseDto
{
    public int Id { get; set; }

    public DateTime PurchaseDate { get; set; } = DateTime.Now;
    public decimal Price { get; set; }
    public int Quota { get; set; }
    public bool Active { get; set; } = true;

    public int CostumerId { get; set; }
    public int ProductId { get; set; }

    public DateTime ConsultedAt { get; set; } = DateTime.Now;
}
