using System.ComponentModel.DataAnnotations;

namespace API.DAL.DTO.Purchases;

public class CreatePurchaseDto
{
    [Required(ErrorMessage = "{0} is required.")]
    public DateTime PurchaseDate { get; set; } = DateTime.Now;

    [Required(ErrorMessage = "{0} is required.")]
    [Range(0.00, 9_999_999.99, ErrorMessage = "{0} should be between {1} and {2}.")]
    public decimal Price { get; set; }

    [Required(ErrorMessage = "{0} is required.")]
    [Range(0, 9_999_999, ErrorMessage = "{0} should be between {1} and {2}.")]
    public int Quota { get; set; }

    [Required(ErrorMessage = "{0} is required.")]
    public bool Active { get; set; } = true;

    [Required(ErrorMessage = "{0} is required.")]
    public int CostumerId { get; set; }

    [Required(ErrorMessage = "{0} is required.")]
    public int ProductId { get; set; }
}
