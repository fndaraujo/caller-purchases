namespace API.DAL.DTO.Products;

public class ReadProductDto
{
    public int Id { get; set; }

    public string Name { get; set; } = String.Empty;
    public int Quota { get; set; }
    public string Description { get; set; } = String.Empty;
    public bool Active { get; set; } = true;
    public DateTime CreatedAt { get; set; } = DateTime.Now;
    public DateTime UpdatedAt { get; set; } = DateTime.Now;

    public DateTime ConsultedAt { get; set; } = DateTime.Now;
}
