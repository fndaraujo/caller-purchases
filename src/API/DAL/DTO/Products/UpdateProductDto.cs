using System.ComponentModel.DataAnnotations;

namespace API.DAL.DTO.Products;

public class UpdateProductDto
{
    [Required(ErrorMessage = "{0} is required.")]
    [StringLength(32, MinimumLength = 1, ErrorMessage = "{0} should be between {2} and {1} characters.")]
    public string Name { get; set; } = String.Empty;

    [Required(ErrorMessage = "{0} is required.")]
    public int Quota { get; set; }

    public string Description { get; set; } = String.Empty;

    [Required(ErrorMessage = "{0} is required.")]
    public bool Active { get; set; } = true;
}
