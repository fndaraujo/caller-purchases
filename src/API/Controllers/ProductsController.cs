using API.DAL;
using API.DAL.DTO.Products;
using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Models.Products;

namespace API.Controllers;

[ApiController]
[Route("/api/[controller]")]
public class ProductsController : ControllerBase
{
    private readonly ApiDbContext _context;
    private readonly IMapper _mapper;

    public ProductsController(ApiDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    [HttpPost]
    public IActionResult Create([FromBody] CreateProductDto productDto)
    {
        var product = _mapper.Map<ProductModel>(productDto);
        _context.Products?.Add(product);
        _context.SaveChanges();
        var result = CreatedAtAction(nameof(GetById), new { id = product.Id }, product);
        return result;
    }

    [HttpGet]
    public IEnumerable<ReadProductDto> GetAll([FromQuery] int skip = 0, [FromQuery] int take = 10)
    {
        var productQuery = _context.Products?.Skip(skip).Take(take);
        var products = _mapper.Map<List<ReadProductDto>>(productQuery);
        var result = products.ToList();
        return result;
    }

    [HttpGet("{id}")]
    public IActionResult GetById(int id)
    {
        var product = _context.Products?.FirstOrDefault(i => i.Id == id);
        if (product is null) return NotFound();
        var result = _mapper.Map<ReadProductDto>(product);
        return Ok(result);
    }

    [HttpPut("{id}")]
    public IActionResult Update([FromBody] UpdateProductDto productDto, int id)
    {
        var product = _context.Products?.FirstOrDefault(i => i.Id == id);
        if (product is null) return NotFound();
        _mapper.Map(productDto, product);
        _context.SaveChanges();
        return NoContent();
    }

    [HttpPatch("{id}")]
    public IActionResult Patch(JsonPatchDocument<UpdateProductDto> productPatch, int id)
    {
        var product = _context.Products?.FirstOrDefault(i => i.Id == id);
        if (product is null) return NotFound();
        var productUpdate = _mapper.Map<UpdateProductDto>(product);
        productPatch.ApplyTo(productUpdate, ModelState);
        if (!TryValidateModel(productUpdate)) return ValidationProblem(ModelState);
        _mapper.Map(productUpdate, product);
        _context.SaveChanges();
        return NoContent();
    }

    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
        var product = _context.Products?.FirstOrDefault(i => i.Id == id);
        if (product is null) return NotFound();
        _context.Remove(product);
        _context.SaveChanges();
        return NoContent();
    }
}
