using API.DAL;
using API.DAL.DTO.Costumers;
using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Models.Costumers;

namespace API.Controllers;

[ApiController]
[Route("/api/[controller]")]
public class CostumersController : ControllerBase
{
    private readonly ApiDbContext _context;
    private readonly IMapper _mapper;

    public CostumersController(ApiDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    [HttpPost]
    public IActionResult Create([FromBody] CreateCostumerDto costumerDto)
    {
        var costumer = _mapper.Map<CostumerModel>(costumerDto);
        _context.Costumers?.Add(costumer);
        _context.SaveChanges();
        var result = CreatedAtAction(nameof(GetById), new { id = costumer.Id }, costumer);
        return result;
    }

    [HttpGet]
    public IEnumerable<ReadCostumerDto> GetAll([FromQuery] int skip = 0, [FromQuery] int take = 10)
    {
        var costumerQuery = _context.Costumers?.Skip(skip).Take(take);
        var costumers = _mapper.Map<List<ReadCostumerDto>>(costumerQuery);
        var result = costumers.ToList();
        return result;
    }

    [HttpGet("{id}")]
    public IActionResult GetById(int id)
    {
        var costumer = _context.Costumers?.FirstOrDefault(i => i.Id == id);
        if (costumer is null) return NotFound();
        var result = _mapper.Map<ReadCostumerDto>(costumer);
        return Ok(result);
    }

    [HttpPut("{id}")]
    public IActionResult Update([FromBody] UpdateCostumerDto costumerDto, int id)
    {
        var costumer = _context.Costumers?.FirstOrDefault(i => i.Id == id);
        if (costumer is null) return NotFound();
        _mapper.Map(costumerDto, costumer);
        _context.SaveChanges();
        return NoContent();
    }

    [HttpPatch("{id}")]
    public IActionResult Patch(JsonPatchDocument<UpdateCostumerDto> costumerPatch, int id)
    {
        var costumer = _context.Costumers?.FirstOrDefault(i => i.Id == id);
        if (costumer is null) return NotFound();
        var costumerUpdate = _mapper.Map<UpdateCostumerDto>(costumer);
        costumerPatch.ApplyTo(costumerUpdate, ModelState);
        if (!TryValidateModel(costumerUpdate)) return ValidationProblem(ModelState);
        _mapper.Map(costumerUpdate, costumer);
        _context.SaveChanges();
        return NoContent();
    }

    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
        var costumer = _context.Costumers?.FirstOrDefault(i => i.Id == id);
        if (costumer is null) return NotFound();
        _context.Remove(costumer);
        _context.SaveChanges();
        return NoContent();
    }
}
