using API.DAL;
using API.DAL.DTO.Purchases;
using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Models.Purchases;

namespace API.Controllers;

[ApiController]
[Route("/api/[controller]")]
public class PurchasesController : ControllerBase
{
    private readonly ApiDbContext _context;
    private readonly IMapper _mapper;

    public PurchasesController(ApiDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    [HttpPost]
    public IActionResult Create([FromBody] CreatePurchaseDto purchaseDto)
    {
        var purchase = _mapper.Map<PurchaseModel>(purchaseDto);
        _context.Purchases?.Add(purchase);
        _context.SaveChanges();
        var result = CreatedAtAction(nameof(GetById), new { id = purchase.Id }, purchase);
        return result;
    }

    [HttpGet]
    public IEnumerable<ReadPurchaseDto> GetAll([FromQuery] int skip = 0, [FromQuery] int take = 10)
    {
        var purchaseQuery = _context.Purchases?.Skip(skip).Take(take);
        var purchases = _mapper.Map<List<ReadPurchaseDto>>(purchaseQuery);
        var result = purchases.ToList();
        return result;
    }

    [HttpGet("{id}")]
    public IActionResult GetById(int id)
    {
        var purchase = _context.Purchases?.FirstOrDefault(i => i.Id == id);
        if (purchase is null) return NotFound();
        var result = _mapper.Map<ReadPurchaseDto>(purchase);
        return Ok(result);
    }

    [HttpPut("{id}")]
    public IActionResult Update([FromBody] UpdatePurchaseDto purchaseDto, int id)
    {
        var purchase = _context.Purchases?.FirstOrDefault(i => i.Id == id);
        if (purchase is null) return NotFound();
        _mapper.Map(purchaseDto, purchase);
        _context.SaveChanges();
        return NoContent();
    }

    [HttpPatch("{id}")]
    public IActionResult Patch(JsonPatchDocument<UpdatePurchaseDto> purchasePatch, int id)
    {
        var purchase = _context.Purchases?.FirstOrDefault(i => i.Id == id);
        if (purchase is null) return NotFound();
        var purchaseUpdate = _mapper.Map<UpdatePurchaseDto>(purchase);
        purchasePatch.ApplyTo(purchaseUpdate, ModelState);
        if (!TryValidateModel(purchaseUpdate)) return ValidationProblem(ModelState);
        _mapper.Map(purchaseUpdate, purchase);
        _context.SaveChanges();
        return NoContent();
    }

    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
        var purchase = _context.Purchases?.FirstOrDefault(i => i.Id == id);
        if (purchase is null) return NotFound();
        _context.Remove(purchase);
        _context.SaveChanges();
        return NoContent();
    }
}
