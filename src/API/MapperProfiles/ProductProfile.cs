using API.DAL.DTO.Products;
using AutoMapper;
using Models.Products;

namespace API.MapperProfiles;

public class ProductProfile : Profile
{
    public ProductProfile()
    {
        CreateMap<CreateProductDto, ProductModel>();
        CreateMap<ProductModel, ReadProductDto>();
        CreateMap<UpdateProductDto, ProductModel>();
    }
}
