using API.DAL.DTO.Costumers;
using AutoMapper;
using Models.Costumers;

namespace API.MapperProfiles;

public class CostumerProfile : Profile
{
    public CostumerProfile()
    {
        CreateMap<CreateCostumerDto, CostumerModel>();
        CreateMap<CostumerModel, ReadCostumerDto>();
        CreateMap<UpdateCostumerDto, CostumerModel>();
    }
}
