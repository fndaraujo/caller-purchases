using API.DAL.DTO.Purchases;
using AutoMapper;
using Models.Purchases;

namespace API.MapperProfiles;

public class PurchaseProfile : Profile
{
    public PurchaseProfile()
    {
        CreateMap<CreatePurchaseDto, PurchaseModel>();
        CreateMap<PurchaseModel, ReadPurchaseDto>();
        CreateMap<UpdatePurchaseDto, PurchaseModel>();
    }
}
