using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Purchases;

[Table("products_costumers")]
public class PurchaseModel
{
    [Key]
    [Required(ErrorMessage = "{0} is required.")]
    [Column("id")]
    public int Id { get; set; }

    [Required(ErrorMessage = "{0} is required.")]
    [Column("purchase_date")]
    public DateTime PurchaseDate { get; set; } = DateTime.Now;

    [Required(ErrorMessage = "{0} is required.")]
    [Column("price")]
    [Range(0.00, 9_999_999.99, ErrorMessage = "{0} should be between {1} and {2}.")]
    public decimal Price { get; set; }

    [Required(ErrorMessage = "{0} is required.")]
    [Column("quota")]
    [Range(0, 9_999_999, ErrorMessage = "{0} should be between {1} and {2}.")]
    public int Quota { get; set; }

    [Required(ErrorMessage = "{0} is required.")]
    [Column("active")]
    public bool Active { get; set; } = true;

    [Required(ErrorMessage = "{0} is required.")]
    [Column("created_at")]
    public DateTime CreatedAt { get; set; } = DateTime.Now;

    [Required(ErrorMessage = "{0} is required.")]
    [Column("updated_at")]
    public DateTime UpdatedAt { get; set; } = DateTime.Now;

    [Required(ErrorMessage = "{0} is required.")]
    [Column("costumer_id")]
    public int CostumerId { get; set; }

    [Required(ErrorMessage = "{0} is required.")]
    [Column("product_id")]
    public int ProductId { get; set; }
}
