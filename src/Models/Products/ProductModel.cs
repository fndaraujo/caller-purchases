using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Models.Costumers;

namespace Models.Products;

[Table("products")]
public class ProductModel
{
    [Key]
    [Required(ErrorMessage = "{0} is required.")]
    [Column("id")]
    public int Id { get; set; }

    [Required(ErrorMessage = "{0} is required.")]
    [Column("name")]
    [StringLength(32, MinimumLength = 1, ErrorMessage = "{0} should be between {2} and {1} characters.")]
    public string Name { get; set; } = String.Empty;

    [Required(ErrorMessage = "{0} is required.")]
    [Column("quota")]
    public int Quota { get; set; }

    [Column("description")]
    public string Description { get; set; } = String.Empty;

    [Required(ErrorMessage = "{0} is required.")]
    [Column("active")]
    public bool Active { get; set; } = true;

    [Required(ErrorMessage = "{0} is required.")]
    [Column("created_at")]
    public DateTime CreatedAt { get; set; } = DateTime.Now;

    [Required(ErrorMessage = "{0} is required.")]
    [Column("updated_at")]
    public DateTime UpdatedAt { get; set; } = DateTime.Now;

    public virtual ICollection<CostumerModel> Costumers { get; } = new List<CostumerModel>();
}
