using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Models.Products;

namespace Models.Costumers;

[Table("costumers")]
public class CostumerModel
{
    [Key]
    [Required(ErrorMessage = "{0} is required.")]
    [Column("id")]
    public int Id { get; set; }

    [Required(ErrorMessage = "{0} is required.")]
    [Column("name")]
    [StringLength(32, MinimumLength = 1, ErrorMessage = "{0} should be between {2} and {1} characters.")]
    public string Name { get; set; } = String.Empty;

    [Required(ErrorMessage = "{0} is required.")]
    [Column("surname")]
    [StringLength(256, MinimumLength = 1, ErrorMessage = "{0} should be between {2} and {1} characters.")]
    public string Surname { get; set; } = String.Empty;

    [Required(ErrorMessage = "{0} is required.")]
    [Column("birth")]
    [Range(typeof(DateTime), "1900-01-01", "2099-12-31", ErrorMessage = "{0} should be between {2} and {3} dates.")]
    public DateTime Birth { get; set; } = new();

    [Required(ErrorMessage = "{0} is required.")]
    [Column("email")]
    [StringLength(254, ErrorMessage = "{0} should be {1} maximum characters.")]
    public string Email { get; set; } = String.Empty;

    [Column("telephone")]
    [StringLength(10, ErrorMessage = "{0} should be {1} maximum characters.")]
    public string Telephone { get; set; } = String.Empty;

    [Column("mobile")]
    [StringLength(11, ErrorMessage = "{0} should be {1} maximum characters.")]
    public string Mobile { get; set; } = String.Empty;

    [Required(ErrorMessage = "{0} is required.")]
    [Column("active")]
    public bool Active { get; set; } = true;

    [Required(ErrorMessage = "{0} is required.")]
    [Column("created_at")]
    public DateTime CreatedAt { get; set; } = DateTime.Now;

    [Required(ErrorMessage = "{0} is required.")]
    [Column("updated_at")]
    public DateTime UpdatedAt { get; set; } = DateTime.Now;

    public virtual ICollection<ProductModel> Products { get; } = new List<ProductModel>();
}
